@echo off
set targetpath=10mpq_path
for /f "tokens=1,2 delims=:" %%i in ('reg query "HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II" /v "MpqPath"') do (
    set "value1=%%i"
    set "value2=%%j" 
)
set targetpath="%value1:~-1%:%value2%"
@rem eg. set targetpath="D:\D2SE"，那么这个D:\D2SE目录下面就要有10mpq

del D2CHAR.mpq
mklink D2CHAR.mpq %targetpath%\D2CHAR.mpq
del D2data.mpq
mklink D2data.mpq %targetpath%\D2data.mpq
del D2EXP.mpq
mklink D2EXP.mpq %targetpath%\D2EXP.mpq
del D2music.mpq
mklink D2music.mpq %targetpath%\D2music.mpq
del D2sfx.mpq
mklink D2sfx.mpq %targetpath%\D2sfx.mpq
del D2SPEECH.mpq
mklink D2SPEECH.mpq %targetpath%\D2SPEECH.mpq
del D2VIDEO.mpq
mklink D2VIDEO.mpq %targetpath%\D2VIDEO.mpq
del D2xmusic.mpq
mklink D2xmusic.mpq %targetpath%\D2xmusic.mpq
del D2XTALK.mpq
mklink D2XTALK.mpq %targetpath%\D2XTALK.mpq
del D2XVIDEO.mpq
mklink D2XVIDEO.mpq %targetpath%\D2XVIDEO.mpq
pause
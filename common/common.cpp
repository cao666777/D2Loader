﻿#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

#pragma comment(lib, "Version.Lib")

static const char* VersionStrings[] = { "1.00","1.07","1.08","1.09","1.09b","1.09d","1.10f","1.11","1.11b","1.12a","1.13c","1.13d","1.14a","1.14b","1.14c","1.14d" };

const char* GetVersionString(int version)
{
    if (version < 0 || version >= sizeof(VersionStrings))
        return "UNKNOWN";
    return VersionStrings[version];
}

int D2Loader_GetVersionId(char *pcVersion)
{
    for ( int i = 0; i < sizeof(VersionStrings) / sizeof(VersionStrings[0]); ++i )
    {
        if ( !_stricmp(pcVersion, VersionStrings[i]) )
        {
            return i;
        }
    }

    return -1;
}

static eGameVersion GetD2Version(LPCVOID pVersionResource)
{
    if (!pVersionResource) return UNKNOWN;

    UINT uLen;
    VS_FIXEDFILEINFO* ptFixedFileInfo;
    if (!VerQueryValue(pVersionResource, "\\", (LPVOID*)&ptFixedFileInfo, &uLen))
        return UNKNOWN;

    if (uLen == 0)
        return UNKNOWN;

    WORD major = HIWORD(ptFixedFileInfo->dwFileVersionMS);
    WORD minor = LOWORD(ptFixedFileInfo->dwFileVersionMS);
    WORD revision = HIWORD(ptFixedFileInfo->dwFileVersionLS);
    WORD subrevision = LOWORD(ptFixedFileInfo->dwFileVersionLS);

    if (major != 1)
        return UNKNOWN;
    if (minor == 0 && revision == 7 && subrevision == 0) return V107;
    if (minor == 0 && revision == 8 && subrevision == 28) return V108;
    if (minor == 0 && revision == 9 && subrevision == 19) return V109;
    if (minor == 0 && revision == 9 && subrevision == 20) return V109b;
    if (minor == 0 && revision == 9 && subrevision == 22) return V109d;
    if (minor == 0 && revision == 10 && subrevision == 39) return V110f;
    if (minor == 0 && revision == 11 && subrevision == 45) return V111;
    if (minor == 0 && revision == 11 && subrevision == 46) return V111b;
    if (minor == 0 && revision == 12 && subrevision == 49) return V112a;
    if (minor == 0 && revision == 13 && subrevision == 60) return V113c;
    if (minor == 0 && revision == 13 && subrevision == 64) return V113d;
    if (minor == 14 && revision == 0 && subrevision == 64) return V114a;
    if (minor == 14 && revision == 1 && subrevision == 68) return V114b;
    if (minor == 14 && revision == 2 && subrevision == 70) return V114c;
    if (minor == 14 && revision == 3 && subrevision == 71) return V114d;
    return UNKNOWN;
}

eGameVersion GetD2Version(const char* gameExe)
{
    static eGameVersion version = UNKNOWN;

    if ( UNKNOWN != version )
    {
        return version;
    }

    DWORD len = GetFileVersionInfoSize(gameExe, NULL);
    if (len == 0)
        return UNKNOWN;

    BYTE* pVersionResource = new BYTE[len];
    GetFileVersionInfo(gameExe, NULL, len, pVersionResource);
    version = GetD2Version(pVersionResource);
    delete pVersionResource;

    return version;
}

int msgBox(LPCSTR boxName, UINT uType, LPCSTR pFormat, ...)
{
    char buffer[300];
    va_list lArgs;
    va_start( lArgs, pFormat );
    vsprintf_s( buffer, sizeof(buffer), pFormat, lArgs );
    va_end(lArgs);

    return MessageBox(NULL, buffer, boxName, uType);
}

